package com.example.nearby_place_finder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Lord_one extends AppCompatActivity {

    Button start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lord_one);

        start=findViewById(R.id.btnstart);



        Thread thread=new Thread() {
            @Override
            public void run() {
                try {

                    sleep(50000000);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                finally
                {

                    Intent intent=new Intent(Lord_one.this,MapsActivity.class);
                    startActivity(intent);
                }

            }

        };

        thread.start();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(Lord_one.this,MapsActivity.class);
                startActivity(intent);
                finish();
            }
        });




    }





}
