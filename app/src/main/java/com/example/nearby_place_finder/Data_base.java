package com.example.nearby_place_finder;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Data_base extends AppCompatActivity {

    database_helper mydb;

    Button button;
    Button button1;
    Button button2;
    Button button3;

    EditText textname;
    EditText textsname;
    EditText textmarks;
    EditText textid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);

        mydb=new database_helper(this);

        button=(Button)findViewById(R.id.btnback);
        button1=(Button)findViewById(R.id.btnview);
        button3=(Button)findViewById(R.id.btndel);

        textid=(EditText)findViewById(R.id.textid);



        // adddata();
        DeleteData();
        viewAll();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(Data_base.this,MapsActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }



    public void viewAll() {
        button1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = mydb.getAllData();
                        if(res.getCount() == 0) {
                            // show message
                            showMessage("Error","Nothing found");
                            return;
                        }

                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("Id :"+ res.getString(0)+"\n");
                            buffer.append("endlatitude :"+ res.getString(1)+"\n");
                            buffer.append("endlongitude :"+ res.getString(2)+"\n");
                            buffer.append("Distranse :"+ res.getString(3)+"\n\n");
                        }

                        // Show all data
                        showMessage("Data",buffer.toString());
                    }
                }
        );
    }

    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }





    public void DeleteData() {
        button3.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer deletedRows = mydb.deleteData(textid.getText().toString());
                        if(deletedRows > 0)
                            Toast.makeText(Data_base.this,"Data Deleted",Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(Data_base.this,"Data not Deleted",Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

}
